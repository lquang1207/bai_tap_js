/*
input : chiều dài + rộng hình chữ nhật 
Steps:
b1: nhập input 
b2 : chu vi = ( dài + rộng )*2 
b3: diện tích = dài * rộng 
b4: xuất kết quả 
output : chu vi , diện tích  
*/
var chieuDai = 10;
var chieuRong = 5;
var chuVi = (chieuDai+chieuRong)*2;
var dienTich = chieuDai*chieuRong;
console.log("chu vi la: " + chuVi ,"diện tích là : " + dienTich);